task fill_ancestry: :environment do

  Classificator.all.each do |classificator|
    if classificator[:parent_id] != nil
      classificator.update_attribute :parent, Classificator.find(classificator[:parent_id])
    end
    p classificator[:ancestry]
    #classificator.save
  end

  #Classificator.save
end
