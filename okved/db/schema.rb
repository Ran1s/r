# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150731094813) do

  create_table "classificators", force: :cascade do |t|
    t.string  "code",                        null: false
    t.string  "name",                        null: false
    t.text    "additional_info"
    t.integer "parent_id"
    t.string  "parent_code"
    t.integer "node_count",      default: 0
    t.string  "ancestry"
  end

  add_index "classificators", ["ancestry"], name: "index_classificators_on_ancestry"
  add_index "classificators", ["parent_id"], name: "index_classificators_on_parent_id"

end
