class AddAncestryToClassificators < ActiveRecord::Migration
  def change
    add_column :classificators, :ancestry, :string
    add_index :classificators, :ancestry
  end
end
