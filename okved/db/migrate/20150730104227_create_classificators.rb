class CreateClassificators < ActiveRecord::Migration
  def change
    create_table :classificators do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.text :additional_info
      t.references :parent, null: true, default: nil, index: true
      t.string :parent_code, null: true, default: nil
      t.integer :node_count, default: 0
    end
  end
end
