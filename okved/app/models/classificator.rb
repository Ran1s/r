class Classificator < ActiveRecord::Base
  has_ancestry
  validates :code, uniqueness: true
  has_many :classificators, class_name: "Classificator", foreign_key: "parent_id"
  belongs_to :classificator, class_name: "Classificator"
end
